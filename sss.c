#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/sha.h>
#include <time.h>

unsigned char* hexToBytes(const char* hex, size_t* length) {
    size_t hexLen = strlen(hex);
    *length = hexLen / 2;
    unsigned char* bytes = malloc(*length);
    for (size_t i = 0; i < hexLen; i += 2) {
        sscanf(hex + i, "%2hhx", &bytes[i / 2]);
    }
    return bytes;
}

void bytesToHex(const unsigned char* bytes, size_t length, char* hex) {
    for (size_t i = 0; i < length; i++) {
        sprintf(hex + (i * 2), "%02x", bytes[i]);
    }
}

void bits2target(const char* bits, unsigned char* target) {
    unsigned char bitsArr[4];
    sscanf(bits, "%08x", (unsigned int*) &bitsArr);
    unsigned char shift = bitsArr[0] - 3;
    memcpy(target + shift, bitsArr + 1, 3);
}

void make_header(const char* previousBlockHash, const char* merkleRoot, const char* bits, unsigned int nonce, unsigned char* header) {
    unsigned char versionArr[4];
    sscanf("54525952", "%08x", (unsigned int*) &versionArr);
    memcpy(header, versionArr, 4);
    size_t length;
    unsigned char* previousBlockBytes = hexToBytes(previousBlockHash, &length);
    for (int i = 0; i < 32; i++) {
        header[4 + i] = previousBlockBytes[31 - i];
    }
    free(previousBlockBytes);
    unsigned char* merkleRootBytes = hexToBytes(merkleRoot, &length);
    for (int i = 0; i < 32; i++) {
        header[36 + i] = merkleRootBytes[31 - i];
    }
    free(merkleRootBytes);
    sscanf(bits, "%08x", (unsigned int*) (header + 68));
    memcpy(header + 76, &nonce, 4);
}


void compute_raw_hash(unsigned char* header, unsigned char* hash) {
    SHA256(SHA256(header, 80, hash), SHA256_DIGEST_LENGTH, hash);
}

void nonceFinder() {
    const char* previousBlockHash = "0000000000000000000b5060c3f578edcad8ff1e76df460d96227e34228d8420";
    const char* merkleRoot = "19aec47aaa99320f2a3b6f76b35d08ba6808f7bd0ff5920f949c29c6df613ed2";
    const char* bits = "170e2632";
    unsigned char header[80];
    unsigned char hash[32];
    unsigned char target[32];
    unsigned int nonce = 2422853640;
    unsigned int max_nonce = 0xffffffff;
    size_t targetLen;

    bits2target(bits, target);

    double hash_rate = 0.0;
    int hash_rate_count = 0;
    time_t time_stamp = time(NULL);

    while (nonce <= max_nonce) {
        make_header(previousBlockHash, merkleRoot, bits, nonce, header);
        compute_raw_hash(header, hash);

        int is_valid = 1;
        for (int i = 0; i < 32; i++) {
            if (hash[i] > target[i]) {
                is_valid = 0;
                break;
            } else if (hash[i] < target[i]) {
                break;
            }
        }

        if (is_valid) {
            char hex[65];
            bytesToHex(hash, 32, hex);
            printf("Nonce: %u\nHash: %s\n", nonce, hex);
            return;
        }

        nonce++;
        hash_rate_count++;
        if (hash_rate_count == 100000) {
            double elapsed_time = difftime(time(NULL), time_stamp);
            hash_rate = hash_rate_count / elapsed_time;
            printf("Hash rate: %.2lf hashes/s\n", hash_rate);
            hash_rate_count = 0;
            time_stamp = time(NULL);
        }
    }
}


int main() {
    // your code here

    nonceFinder();

    return 0;
}