import pycuda.autoinit
import pycuda.driver as cuda
import pycuda.gpuarray as gpuarray
import numpy as np
import cuSha256

# Initialize cuSHA256 library
cuSha256.cuSha256Init()

# Create input data array
data = np.array([ord(c) for c in "hello world"], dtype=np.uint8)

# Allocate GPU memory and copy input data to GPU
data_gpu = gpuarray.to_gpu(data)

# Compute SHA-256 hash on GPU
hash_gpu = cuSha256.cuSha256(data_gpu)

# Copy hash result from GPU to CPU
hash_result = hash_gpu.get()

# Print hash result
print("Hash: ", end="")
for i in range(32):
    print("%02x" % hash_result[i], end="")
print()

# Clean up
cuSha256.cuSha256Cleanup()
