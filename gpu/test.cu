#include <stdio.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <openssl/sha.h>

__device__ void sha(const char* message, unsigned char* hash) {
    SHA256_CTX sha256;
    SHA256_Init(&sha256);
    SHA256_Update(&sha256, message, strlen(message));
    SHA256_Final(hash, &sha256);
}

__global__ void test_sha(const char* message) {
    unsigned char hash[SHA256_DIGEST_LENGTH];
    sha(message, hash);
    printf("Message: %s\nHash: ", message);
    for (int i = 0; i < SHA256_DIGEST_LENGTH; i++) {
        printf("%02x", hash[i]);
    }
    printf("\n");
}

int main() {
    const char* message = "Hello, World!";
    test_sha<<<1, 1>>>(message);
    cudaDeviceSynchronize();
    return 0;
}
