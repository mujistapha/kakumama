import numpy as np


def RR(x, y):
    TR = np.zeros(shape=len(x), dtype=str)
    for i in range(len(x)):
        TR[i] = x[i - (y % len(x))]
    TR2 = ''
    for i in TR:
        TR2 += str(i)
    return TR2


def ror(x, y):
    x_ = bin(x).replace('0b', '')
    x_ = '0' * (32 - len(x_)) + x_
    TR = RR(x_, y)
    return int(TR, 2)


def RS(x, y):
    rr = RR(x, y)
    TR = ''
    for i in range(len(x)):
        if i < y:
            TR += '0'
        else:
            TR += rr[i]
    return TR


def XOR(x, y, z):
    TR = ''
    for i in range(len(x)):
        TR += str(int(x[i]) ^ int(y[i]) ^ int(z[i]))
    return TR


def xor(a, b, c):
    a_ = bin(a).replace('0b', '')
    a_ = '0' * (32 - len(a_)) + a_
    b_ = bin(b).replace('0b', '')
    b_ = '0' * (32 - len(b_)) + b_
    c_ = bin(c).replace('0b', '')
    c_ = '0' * (32 - len(c_)) + c_
    TR = int(XOR(a_, b_, c_), 2)
    return TR


def NOT(dec):
    d_ = bin(dec).replace('0b', '')
    d_ = '0' * (32 - len(d_)) + d_
    temp = ''
    for i in d_:
        if i == '1':
            temp += '0'
        else:
            temp += '1'
    return int(temp, 2)


def AND(a, b):
    a_ = bin(a).replace('0b', '')
    a_ = '0' * (32 - len(a_)) + a_
    b_ = bin(b).replace('0b', '')
    b_ = '0' * (32 - len(b_)) + b_
    temp = ''
    for i in range(32):
        if a_[i] == b_[i] and a_[i] == '1':
            temp += '1'
        else:
            temp += '0'
    return int(temp, 2)


def xor_2(a, b):
    a_ = bin(a).replace('0b', '')
    a_ = '0' * (32 - len(a_)) + a_
    b_ = bin(b).replace('0b', '')
    b_ = '0' * (32 - len(b_)) + b_
    temp = ''
    for i in range(32):
        temp += str(int(a_[i]) ^ int(b_[i]))
    return int(temp, 2)


def sum_(w, x, y, z):
    s = bin((int(w, 2) + int(x, 2) + int(y, 2) + int(z, 2)) % (2 ** 32)).replace("0b", "")
    if len(s) != 32:
        return '0' * (32 - len(s)) + s
    return s


n = []


def sha(data):
    h0 = 0x6a09e667
    h1 = 0xbb67ae85
    h2 = 0x3c6ef372
    h3 = 0xa54ff53a
    h4 = 0x510e527f
    h5 = 0x9b05688c
    h6 = 0x1f83d9ab
    h7 = 0x5be0cd19

    K = np.array([0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
                  0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
                  0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
                  0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
                  0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
                  0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
                  0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
                  0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
                  ])

    binary = ''.join(format(x, '08b') for x in data)
    l = len(binary)
    binary += '1'
    binary += '0' * (512 - len(binary) - 64)
    a = bin(l).replace('0b', '')
    a = (64 - len(a)) * '0' + a
    m = binary + a + '0' * (48 * 32)
    shed = np.array([m[i:i + 32] for i in range(0, 2048, 32)])

    for i in range(16, 64):
        s0 = XOR(RR(shed[i - 15], 7), RR(shed[i - 15], 18), RS(shed[i - 15], 3))
        s1 = XOR(RR(shed[i - 2], 17), RR(shed[i - 2], 19), RS(shed[i - 2], 10))
        z = sum_(shed[i - 16], s0, shed[i - 7], s1)
        shed[i] = z

    a = h0
    b = h1
    c = h2
    d = h3
    e = h4
    f = h5
    g = h6
    h = h7

    for i in range(64):
        S1 = xor(ror(e, 6), ror(e, 11), ror(e, 25))
        ch = xor_2(AND(e, f), AND(NOT(e), g))
        temp1 = (h + S1 + ch + K[i] + int(shed[i], 2)) % (2 ** 32)
        S0 = xor(ror(a, 2), ror(a, 13), ror(a, 22))
        maj = xor(AND(a, b), AND(a, c), AND(b, c))
        temp2 = (S0 + maj % (2 ** 32))
        h = g % (2 ** 32)
        g = f % (2 ** 32)
        f = e % (2 ** 32)
        e = (d + temp1) % (2 ** 32)
        d = c % (2 ** 32)
        c = b % (2 ** 32)
        b = a % (2 ** 32)
        a = (temp1 + temp2) % (2 ** 32)

    h0 = (h0 + a) % (2 ** 32)
    h1 = (h1 + b) % (2 ** 32)
    h2 = (h2 + c) % (2 ** 32)
    h3 = (h3 + d) % (2 ** 32)
    h4 = (h4 + e) % (2 ** 32)
    h5 = (h5 + f) % (2 ** 32)
    h6 = (h6 + g) % (2 ** 32)
    h7 = (h7 + h) % (2 ** 32)

    h0 = bin(h0).replace('0b', '')
    h1 = bin(h1).replace('0b', '')
    h2 = bin(h2).replace('0b', '')
    h3 = bin(h3).replace('0b', '')
    h4 = bin(h4).replace('0b', '')
    h5 = bin(h5).replace('0b', '')
    h6 = bin(h6).replace('0b', '')
    h7 = bin(h7).replace('0b', '')

    h0 = (32 - len(h0)) * '0' + h0
    h1 = (32 - len(h1)) * '0' + h1
    h2 = (32 - len(h2)) * '0' + h2
    h3 = (32 - len(h3)) * '0' + h3
    h4 = (32 - len(h4)) * '0' + h4
    h5 = (32 - len(h5)) * '0' + h5
    h6 = (32 - len(h6)) * '0' + h6
    h7 = (32 - len(h7)) * '0' + h7

    TR = h0 + h1 + h2 + h3 + h4 + h5 + h6 + h7
    spl = [TR[i:i + 8] for i in range(0, len(TR), 8)]
    z = ''
    for i in spl:
        dec = int(i, 2)
        hex_ = hex(dec).replace('0x', '')
        z += ('0' * (2 - len(hex_)) + hex_)
    return z


import hashlib as ha

a = sha(b'hello world')
print(a)
b = ha.sha256(b'hello world').hexdigest()
print(b)
assert a == b

print("================================================")

