from numba import cuda
import numpy as np
from numba import int16, int32
from math import pow

@cuda.jit(device=True)
def rightRot(TR, x, y):
    for i in range(len(x)):
        TR[i] = x[i - (y % len(x))]
    return TR

@cuda.jit(device=True)
def rightShift(TR, x, y):
    for i in range(len(x)):
        TR[i] = x[i - (y % len(x))]
    for i in range(y % len(x)):
        TR[i] = 0
    return TR

@cuda.jit(device=True)
def XOR_3(TR, a, b, c):
    for i in range(len(a)):
        TR[i] = a[i] ^ b[i] ^ c[i]
    return TR

@cuda.jit(device=True)
def binary(TR, num):
    count = len(TR) - 1
    c = num
    while c != 0:
        TR[count] = c % 2
        c = (c - (c % 2)) / 2
        count -= 1
    return TR

@cuda.jit(device=True)
def decimal(x):
    TR = 0
    for i in range(len(x)):
        TR += x[len(x) - i - 1] * pow(2, i)
    return TR

@cuda.jit(device=True)
def ror(TR, dec, y):
    TR = int(dec) >> int(y)
    return TR

@cuda.jit(device=True)
def SHA256(TR, message, lenght):

    INITIAL_HASH = cuda.local.array(shape=(8), dtype=int32)
    INITIAL_HASH[0] = 1779033703
    INITIAL_HASH[1] = 3144134277
    INITIAL_HASH[2] = 1013904242
    INITIAL_HASH[3] = 2773480762
    INITIAL_HASH[4] = 1359893119
    INITIAL_HASH[5] = 2600822924
    INITIAL_HASH[6] = 528734635
    INITIAL_HASH[7] = 1541459225

    K256 = cuda.local.array(shape=(64), dtype=int32)
    K256[0] = 1116352408
    K256[1] = 1899447441
    K256[2] = 3049323471
    K256[3] = 3921009573
    K256[4] = 961987163
    K256[5] = 1508970993
    K256[6] = 2453635748
    K256[7] = 2870763221
    K256[8] = 3624381080
    K256[9] = 310598401
    K256[10] = 607225278
    K256[11] = 1426881987
    K256[12] = 1925078388
    K256[13] = 2162078206
    K256[14] = 2614888103
    K256[15] = 3248222580
    K256[16] = 3835390401
    K256[17] = 4022224774
    K256[18] = 264347078
    K256[19] = 604807628
    K256[20] = 770255983
    K256[21] = 1249150122
    K256[22] = 1555081692
    K256[23] = 1996064986
    K256[24] = 2554220882
    K256[25] = 2821834349
    K256[26] = 2952996808
    K256[27] = 3210313671
    K256[28] = 3336571891
    K256[29] = 3584528711
    K256[30] = 113926993
    K256[31] = 338241895
    K256[32] = 666307205
    K256[33] = 773529912
    K256[34] = 1294757372
    K256[35] = 1396182291
    K256[36] = 1695183700
    K256[37] = 1986661051
    K256[38] = 2177026350
    K256[39] = 2456956037
    K256[40] = 2730485921
    K256[41] = 2820302411
    K256[42] = 3259730800
    K256[43] = 3345764771
    K256[44] = 3516065817
    K256[45] = 3600352804
    K256[46] = 4094571909
    K256[47] = 275423344
    K256[48] = 430227734
    K256[49] = 506948616
    K256[50] = 659060556
    K256[51] = 883997877
    K256[52] = 958139571
    K256[53] = 1322822218
    K256[54] = 1537002063
    K256[55] = 1747873779
    K256[56] = 1955562222
    K256[57] = 2024104815
    K256[58] = 2227730452
    K256[59] = 2361852424
    K256[60] = 2428436474
    K256[61] = 2756734187
    K256[62] = 3204031479
    K256[63] = 3329325298

    k = 0
    while (lenght + 1 + k) % 512 != 0:
        k += 1
    total_lenght = lenght + 1 + k
    hash = cuda.local.array(shape=(512), dtype=int16)
    for i in range(lenght):
        hash[i] = message[i]
    hash[lenght] = 1
    for i in range(lenght + 1, total_lenght - 64):
        hash[i] = 0
    bin_ = cuda.local.array(shape=(64), dtype=int16)
    bin_ = binary(bin_, lenght)
    for i in range(64):
        hash[total_lenght - 64 + i] = bin_[i]

    new = cuda.local.array(shape=(64, 32), dtype=int16)
    for i in range(16):
        for j in range(32):
            new[i][j] = hash[i * 32 + j]
    for i in range(48):
        for j in range(32):
            new[i + 16][j] = 0

    modulo = pow(2, 32)

    for i in range(16, 64):
        TEMP = cuda.local.array(shape=(32), dtype=int16)
        TEMP1 = cuda.local.array(shape=(32), dtype=int16)
        TEMP2 = cuda.local.array(shape=(32), dtype=int16)
        TEMP3 = cuda.local.array(shape=(32), dtype=int16)
        s0 = XOR_3(TEMP3, rightRot(TEMP, new[i - 15], 7), rightRot(TEMP1, new[i - 15], 18), rightShift(TEMP2, new[i - 15], 3))
        TEMP = cuda.local.array(shape=(32), dtype=int16)
        TEMP1 = cuda.local.array(shape=(32), dtype=int16)
        TEMP2 = cuda.local.array(shape=(32), dtype=int16)
        TEMP3 = cuda.local.array(shape=(32), dtype=int16)
        s1 = XOR_3(TEMP3, rightRot(TEMP, new[i - 2], 17), rightRot(TEMP1, new[i - 2], 19), rightShift(TEMP2, new[i - 2], 10))
        z = (decimal(new[i - 16]) + decimal(s0) + decimal(new[i - 7]) + decimal(s1)) % modulo
        TEMP = cuda.local.array(shape=(32), dtype=int16)
        y = binary(TEMP, z)
        for j in range(32):
            new[i][j] = y[j]

    a = INITIAL_HASH[0]
    b = INITIAL_HASH[1]
    c = INITIAL_HASH[2]
    d = INITIAL_HASH[3]
    e = INITIAL_HASH[4]
    f = INITIAL_HASH[5]
    g = INITIAL_HASH[6]
    h = INITIAL_HASH[7]

    for i in range(64):
        TEMP = 0
        TEMP1 = 0
        TEMP2 = 0
        S1 = ror(TEMP, e, 6) ^ ror(TEMP1, e, 11) ^ ror(TEMP2, e, 25)
        ch = (int(e) & int(f)) ^ ((~int(e)) & int(g))
        temp1 = (((((((h + S1) % modulo) + ch) % modulo) + K256[i]) % modulo) + decimal(new[i])) % modulo # doing the calculations at once causes overflows
        TEMP = 0
        TEMP1 = 0
        TEMP2 = 0
        S0 = ror(TEMP, a, 2) ^ ror(TEMP1, a, 13) ^ ror(TEMP2, a, 22)
        maj = (int(a) & int(b)) ^ (int(a) & int(c)) ^ (int(b) & int(c))
        temp2 = (maj + S0) % modulo
        h = g
        g = f
        f = e
        e = (d + temp1) % modulo
        d = c
        c = b
        b = a
        a = (temp1 + temp2) % modulo

    a = a % modulo
    b = b % modulo
    c = c % modulo
    d = d % modulo
    e = e % modulo
    f = f % modulo
    g = g % modulo
    h = h % modulo

    t0 = (INITIAL_HASH[0] + a) % modulo
    t1 = (INITIAL_HASH[1] + b) % modulo
    t2 = (INITIAL_HASH[2] + c) % modulo
    t3 = (INITIAL_HASH[3] + d) % modulo
    t4 = (INITIAL_HASH[4] + e) % modulo
    t5 = (INITIAL_HASH[5] + f) % modulo
    t6 = (INITIAL_HASH[6] + g) % modulo
    t7 = (INITIAL_HASH[7] + h) % modulo

    TEMP = cuda.local.array(shape=(32), dtype=int16)
    t0 = binary(TEMP, t0)
    TEMP = cuda.local.array(shape=(32), dtype=int16)
    t1 = binary(TEMP, t1)
    TEMP = cuda.local.array(shape=(32), dtype=int16)
    t2 = binary(TEMP, t2)
    TEMP = cuda.local.array(shape=(32), dtype=int16)
    t3 = binary(TEMP, t3)
    TEMP = cuda.local.array(shape=(32), dtype=int16)
    t4 = binary(TEMP, t4)
    TEMP = cuda.local.array(shape=(32), dtype=int16)
    t5 = binary(TEMP, t5)
    TEMP = cuda.local.array(shape=(32), dtype=int16)
    t6 = binary(TEMP, t6)
    TEMP = cuda.local.array(shape=(32), dtype=int16)
    t7 = binary(TEMP, t7)

    for i in range(32):
        TR[i] = t0[i]
        TR[i + 32] = t1[i]
        TR[i + 64] = t2[i]
        TR[i + 94] = t3[i]
        TR[i + 128] = t4[i]
        TR[i + 160] = t5[i]
        TR[i + 192] = t6[i]
        TR[i + 224] = t7[i]

    return TR

@cuda.jit
def kernel(nonce, TR, message):
    index = cuda.grid(1)
    TEMP = cuda.local.array(shape=(256), dtype=int16)
    a = SHA256(TEMP, message, len(message))
    for i in range(256):
        TR[i] = int(a[i])

TR = cuda.to_device(np.zeros(shape=256, dtype=np.int16))
message = np.array([0, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0])
message = cuda.to_device(message)
kernel[1, 1](1, TR, message)
cuda.synchronize()
TR = TR.copy_to_host()
print("<TR>")
print(TR)
print("<TR>")
a = ""
for i in range(256):
    a += str(TR[i])


out = []
for i in range(32):
    b = ""
    for j in range(8):
        b += str(TR[i * 8 + j])
    out.append(b)

def hex_(a):
    T = ['0', '0']
    h = str(hex(a)).replace("0x", "")
    if len(h) != 2:
        T[1] = h
    else:
        T[0] = h[0]
        T[1] = h[1]
    return T[0] + T[1]

out_ = ''
for i in out:
    temp = int(i, 2)
    print("-- ", temp)
    out_ += hex_(temp)

print(out_)

import hashlib as ha
# assert out_ == ha.sha256("hello world".encode("ascii")).hexdigest()
print(ha.sha256("hello world".encode("ascii")).hexdigest())