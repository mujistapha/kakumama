import hashlib
import struct
import codecs
import time
import pycuda.driver as cuda
import pycuda.autoinit
import numpy as np

import hashlib
import struct
import codecs
import time
import cupy as cp

import numpy as np

from numba import cuda, uint32, uint8
import numba as nb

@nb.record
class BlockHeader(object):
    version: nb.uint32
    previousblockhash: nb.uint256
    merkleroot: nb.uint256
    time: nb.uint32
    bits: nb.uint32
    nonce: nb.uint32
    nextblockhash: nb.uint256


def bits2target(bits):
    bits = bytes.fromhex(bits)
    shift = bits[0] - 3
    value = bits[1:]
    target = value + b"\x00" * shift
    target = b"\x00" * (32 - len(target)) + target
    return target

def make_header(block):
    header = b""
    header += struct.pack("<L", block['version'])
    header += bytes.fromhex(block['previousblockhash'])[::-1]
    header += bytes.fromhex(block['merkleroot'])[::-1]
    header += struct.pack("<L", block['time'])
    header += bytes.fromhex(block['bits'])[::-1]
    header += struct.pack("<L", block['nonce'])
    return header


def compute_raw_hash(header):
    return hashlib.sha256(hashlib.sha256(header).digest()).digest()[::-1]



@cuda.jit(device=True)
def sha256(block, digest):
    w = np.zeros(64, dtype=np.uint32)

    # initialize hash values
    h0 = 0x6a09e667
    h1 = 0xbb67ae85
    h2 = 0x3c6ef372
    h3 = 0xa54ff53a
    h4 = 0x510e527f
    h5 = 0x9b05688c
    h6 = 0x1f83d9ab
    h7 = 0x5be0cd19

    # initialize constants
    k = np.array([
        0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
        0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
        0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
        0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
        0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
        0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
        0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
        0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
    ], dtype=np.uint32)

    # compute message schedule
    for i in range(16):
        w[i] = (block[i * 4] << 24) | (block[i * 4 + 1] << 16) | (block[i * 4 + 2] << 8) | block[i * 4 + 3]
    for i in range(16, 64):
        s0 = cuda.rotate_right(w[i - 15], 7) ^ cuda.rotate_right(w[i - 15], 18) ^ (w[i - 15] >> 3)
        s1 = cuda.rotate_right(w[i - 2], 17) ^ cuda.rotate_right(w[i - 2], 19) ^ (w[i - 2] >> 10)
        w[i] = w[i - 16] + s0 + w[i - 7] + s1

    # initialize hash values
    h0, h1, h2, h3, h4, h5, h6, h7 = digest

    # main loop
    for i in range(64):
        s1 = cuda.rotate_right(h4, 6) ^ cuda.rotate_right(h4, 11) ^ cuda.rotate_right(h4, 25)
        ch = (h4 & h5) ^ (~h4 & h6)
        temp1 = h7 + s1 + ch + k[i] + w[i]
        s0 = cuda.rotate_right(h0, 2) ^ cuda.rotate_right(h0, 13) ^ cuda.rotate_right(h0, 22)
        maj = (h0 & h1) ^ (h0 & h2) ^ (h1 & h2)
        temp2 = s0 + maj

        h7 = h6
        h6 = h5
        h5 = h4
        h4 = h3 + temp1
        h3 = h2
        h2 = h1
        h1 = h0
        h0 = temp1 + temp2

    # compute digest
    digest[0] += h0
    digest[1] += h1
    digest[2] += h2
    digest[3] += h3
    digest[4] += h4
    digest[5] += h5
    digest[6] += h6
    digest[7] += h7
    return np.array([h0, h1, h2, h3, h4, h5, h6, h7], dtype=np.uint32)


@cuda.jit(device=True)
def gpuMakeHeader(block):
    version = cp.array([block['version']], dtype=cp.uint32)
    previousblockhash = cp.array(bytearray.fromhex(block['previousblockhash'])[::-1], dtype=cp.uint8)
    merkleroot = cp.array(bytearray.fromhex(block['merkleroot'])[::-1], dtype=cp.uint8)
    time = cp.array([block['time']], dtype=cp.uint32)
    bits = cp.array(bytearray.fromhex(block['bits'])[::-1], dtype=cp.uint8)
    nonce = cp.array([block['nonce']], dtype=cp.uint32)

    header = cp.hstack((version.view(cp.uint8), previousblockhash, merkleroot, time.view(cp.uint8), bits,
                         nonce.view(cp.uint8))).tobytes()

    return header

@cuda.jit
def sha256_kernel(block, result):
    # Get the global thread ID
    tx = cuda.threadIdx.x
    ty = cuda.threadIdx.y
    bx = cuda.blockIdx.x
    by = cuda.blockIdx.y
    bw = cuda.blockDim.x
    bh = cuda.blockDim.y
    gx = tx + bx * bw
    gy = ty + by * bh
    idx = gx + gy * cuda.gridDim.x * cuda.blockDim.x

    # Compute the SHA-256 hash for this block header
    header = gpuMakeHeader(block)
    block = header[idx:idx+64]
    digest = np.array([
        0x6a09e667,
        0xbb67ae85,
        0x3c6ef372,
        0xa54ff53a,
        0x510e527f,
        0x9b05688c,
        0x1f83d9ab,
        0x5be0cd19], dtype=np.uint32)
    sha256(block, digest)

    # Write the result back to global memory
    for i in range(8):
        result[idx * 8 + i] = digest[i]


def nonceFinder():
    block = {
        "version": 545259524,
        "merkleroot": "19aec47aaa99320f2a3b6f76b35d08ba6808f7bd0ff5920f949c29c6df613ed2",
        "nonce": 2422853640,
        "bits": "170e2632",
        "time": 1634406908,
        "previousblockhash": "0000000000000000000b5060c3f578edcad8ff1e76df460d96227e34228d8420",
        "nextblockhash": "00000000000000000004cb60d643c690706f7d2bed042236d43ef13c0747b683"
    }

    block = BlockHeader(
        version=545259524,
        previousblockhash=0x0000000000000000000b5060c3f578edcad8ff1e76df460d96227e34228d8420,
        merkleroot=0x19aec47aaa99320f2a3b6f76b35d08ba6808f7bd0ff5920f949c29c6df613ed2,
        time=1634406908,
        bits=0x170e2632,
        nonce=2422853640,
        nextblockhash=0x00000000000000000004cb60d643c690706f7d2bed042236d43ef13c0747b683,
    )



    # Compute SHA-256 hash of the block header using CUDA
    threads_per_block = (1, 1)
    blocks_per_grid = (1, 1)
    result = np.zeros(64 * blocks_per_grid[0] * blocks_per_grid[1], dtype=np.uint32)
    sha256_kernel[blocks_per_grid, threads_per_block](block, result)

    # Print the resulting hash in little-endian byte order
    print(bytearray(result.astype('uint8')).hex())
    print("ggg")

    return

    nonce = block['nonce']  # - 1000000

    time_stamp = time.time()
    hash_rate, hash_rate_count = 0.0, 0
    target = bits2target(block['bits'])

    while nonce <= 0xffffffff:
        block['nonce'] = nonce
        header = make_header(block)
        # header2 = make_header(block)
        hash = compute_raw_hash(header)
        if hash < target:
            print("Block meets target! ", nonce)
            block['nonce'] = nonce
            block['hash'] = codecs.encode(hash, 'hex')
            print("    {:.4f} KH/s\n".format(hash_rate / 1000.0))

            print('Hash: {0}'.format(codecs.encode(hash, 'hex')))
            print('Target: {0}'.format(codecs.encode(target, 'hex')))
            print('Valid Hash: {0}'.format(hash < target))
            return (block, hash_rate)

        if nonce > 0 and nonce % 1048576 == 0:
            hash_rate = hash_rate + ((1048576 / (time.time() - time_stamp)) - hash_rate) / (hash_rate_count + 1)
            hash_rate_count += 1
            print("    {:.4f} KH/s\n".format(hash_rate / 1000.0))
            time_stamp = time.time()

        nonce += 1



    return None



nonceFinder()






