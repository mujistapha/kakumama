import hashlib
import numpy as np
from numba import cuda

# Define the SHA-256 hash function as a device function
@cuda.jit(device=True)
def sha256(data):
    h = hashlib.sha256(data)
    return np.frombuffer(h.digest(), dtype=np.uint8)

# Define a global function that calls the SHA-256 hash function
@cuda.jit
def test_sha256(input_data, output_data):
    i = cuda.grid(1)
    if i < input_data.shape[0]:
        output_data[i] = sha256(input_data[i])



# Create some input data
input_data = np.array([b"hello", b"world"], dtype=np.object_)

# Allocate output data on the device
output_data = cuda.device_array_like(input_data, shape=input_data.shape, dtype=np.uint8)

# Define the block and grid sizes
block_size = 128
grid_size = (input_data.shape[0] + block_size - 1) // block_size

# Call the test_sha256 function
test_sha256[grid_size, block_size](input_data, output_data)

# Print the output data
print(output_data)
