from ctypes import *
import hashlib
import struct
import codecs
import time

import sys

print(type(545259524))

print(struct.pack("<L",545259524))
print(bytes.fromhex("0000000000000000000b5060c3f578edcad8ff1e76df460d96227e34228d8420")[::-1])
print(bytes.fromhex("19aec47aaa99320f2a3b6f76b35d08ba6808f7bd0ff5920f949c29c6df613ed2")[::-1])
print(struct.pack("<L", 1634406908))
print(bytes.fromhex("170e2632")[::-1])
print(struct.pack("<L", 2422853640))

header = b""
header += struct.pack("<L", 545259524)
header += bytes.fromhex("0000000000000000000b5060c3f578edcad8ff1e76df460d96227e34228d8420")[::-1]
header += bytes.fromhex("19aec47aaa99320f2a3b6f76b35d08ba6808f7bd0ff5920f949c29c6df613ed2")[::-1]
header += struct.pack("<L", 1634406908)
header += bytes.fromhex("170e2632")[::-1]

print(header)