import hashlib
import struct
import codecs
import time

def bits2target(bits):
    bits = bytes.fromhex(bits)
    shift = bits[0] - 3
    value = bits[1:]
    target = value + b"\x00" * shift
    target = b"\x00" * (32 - len(target)) + target
    return target

def make_header(block):
    header = b""
    header += struct.pack("<L", block['version'])
    header += bytes.fromhex(block['previousblockhash'])[::-1]
    header += bytes.fromhex(block['merkleroot'])[::-1]
    header += struct.pack("<L", block['time'])
    header += bytes.fromhex(block['bits'])[::-1]
    header += struct.pack("<L", block['nonce'])
    return header


def compute_raw_hash(header):
    return hashlib.sha256(hashlib.sha256(header).digest()).digest()[::-1]


def nonceFinder():
    block = {
        "version": 545259524,
        "merkleroot": "19aec47aaa99320f2a3b6f76b35d08ba6808f7bd0ff5920f949c29c6df613ed2",
        "nonce": 2422853640,
        "bits": "170e2632",
        "time": 1634406908,
        "previousblockhash": "0000000000000000000b5060c3f578edcad8ff1e76df460d96227e34228d8420",
        "nextblockhash": "00000000000000000004cb60d643c690706f7d2bed042236d43ef13c0747b683"
    }

    nonce = block['nonce'] - 1000000

    time_stamp = time.time()
    hash_rate, hash_rate_count = 0.0, 0
    target = bits2target(block['bits'])
    while nonce <= 0xffffffff:
        block['nonce'] = nonce
        header = make_header(block)
        hash = compute_raw_hash(header)

        if hash < target:
            print("Block meets target! ", nonce)
            block['nonce'] = nonce
            block['hash'] = codecs.encode(hash, 'hex')
            print("    {:.4f} KH/s\n".format(hash_rate / 1000.0))

            print('Hash: {0}'.format(codecs.encode(hash, 'hex')))
            print('Target: {0}'.format(codecs.encode(target, 'hex')))
            print('Valid Hash: {0}'.format(hash < target))
            return (block, hash_rate)

        if nonce > 0 and nonce % 1048576 == 0:
            hash_rate = hash_rate + ((1048576 / (time.time() - time_stamp)) - hash_rate) / (hash_rate_count + 1)
            hash_rate_count += 1
            print("    {:.4f} KH/s\n".format(hash_rate / 1000.0))
            time_stamp = time.time()

        nonce += 1

nonceFinder()

