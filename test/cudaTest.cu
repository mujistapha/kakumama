#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <cuda.h>
// #include <cuda_runtime.h>
// #include <openssl/sha.h>
#include <sys/time.h>
#include <pthread.h>
#include <locale.h>
#include "sha256.cuh"

#define SHA256_DIGEST_SIZE 32

typedef struct Block {
    uint32_t version;
    char previousblockhash[64];
    char merkleroot[64];
    uint32_t time;
    char bits[8];
    uint32_t nonce;
} Block;

__device__ void pack_header(uint8_t* header, Block block)
{
    int offset = 0;
    uint32_t tmp;

    // Pack version
    tmp = __builtin_bswap32(block.version);
    memcpy(&header[offset], &tmp, sizeof(tmp));
    offset += sizeof(tmp);

    // Pack previous block hash
    for (int i = 0; i < 32; i++) {
        header[offset + i] = (uint8_t)block.previousblockhash[62 - (i * 2)];
        header[offset + i + 1] = (uint8_t)block.previousblockhash[63 - (i * 2)];
    }
    offset += 32;

    // Pack merkle root
    for (int i = 0; i < 32; i++) {
        header[offset + i] = (uint8_t)block.merkleroot[62 - (i * 2)];
        header[offset + i + 1] = (uint8_t)block.merkleroot[63 - (i * 2)];
    }
    offset += 32;

    // Pack time
    tmp = __builtin_bswap32(block.time);
    memcpy(&header[offset], &tmp, sizeof(tmp));
    offset += sizeof(tmp);

    // Pack bits
    for (int i = 0; i < 4; i++) {
        header[offset + i] = (uint8_t)block.bits[6 - (i * 2)];
        header[offset + i + 1] = (uint8_t)block.bits[7 - (i * 2)];
    }
    offset += 4;

    // Pack nonce
    tmp = __builtin_bswap32(block.nonce);
    memcpy(&header[offset], &tmp, sizeof(tmp));
}
//
// __device__ void compute_raw_hash_kernel(uint8_t* header, uint8_t* raw_hash)
// {
//     SHA256_CTX sha256_ctx;
//     uint8_t hash[SHA256_DIGEST_SIZE];
//
//     SHA256_Init(&sha256_ctx);
//     SHA256_Update(&sha256_ctx, header, 80);
//     SHA256_Final(hash, &sha256_ctx);
//
//     SHA256_Init(&sha256_ctx);
//     SHA256_Update(&sha256_ctx, hash, SHA256_DIGEST_SIZE);
//     SHA256_Final(raw_hash, &sha256_ctx);
// }

__global__ void compute_raw_hash(Block block, uint8_t* raw_hash)
{
    uint8_t* header;
    uint8_t* d_header;
    uint8_t* d_raw_hash;

    header = (uint8_t*)malloc(80);
    pack_header(header, block);

    cudaMalloc(&d_header, 80);
    cudaMalloc(&d_raw_hash, SHA256_DIGEST_SIZE);
    cudaMemcpy(d_header, header, 80, cudaMemcpyHostToDevice);

    printf("Header: %s", header);

//     compute_raw_hash_kernel(d_header, d_raw_hash);

//     SHA256_CTX sha256_ctx;
//     uint8_t hash[SHA256_DIGEST_SIZE];
//
//     SHA256_Init(&sha256_ctx);
//     SHA256_Update(&sha256_ctx, header, 80);
//     SHA256_Final(hash, &sha256_ctx);
//
//     SHA256_Init(&sha256_ctx);
//     SHA256_Update(&sha256_ctx, hash, SHA256_DIGEST_SIZE);
//     SHA256_Final(raw_hash, &sha256_ctx);
//
//     cudaMemcpy(raw_hash, d_raw_hash, SHA256_DIGEST_SIZE, cudaMemcpyDeviceToHost);
//
//     printf("Hash: %s", raw_hash);

    cudaFree(d_header);
    cudaFree(d_raw_hash);
    free(header);
}

int main() {
    Block block;
    block.version = 1;
    strcpy(block.previousblockhash, "0000000000000000000000000000000000000000000000000000000000000000");
    strcpy(block.merkleroot, "9b6f5f7f5b6a3c6d80b24c325f861ef59a3c3e17ff9a92591dcdfbfb55b78a90");
    block.time = 1231469665;
    strcpy(block.bits, "1d00ffff");
    block.nonce = 2573394689;

    uint8_t* raw_hash = (uint8_t*)malloc(SHA256_DIGEST_SIZE);
    compute_raw_hash<<<1,1>>>(block, raw_hash);

    for (int i = 0; i < SHA256_DIGEST_SIZE; i++) {
        printf("%02x", raw_hash[i]);
    }

    printf("\n");
    free(raw_hash);
    return 0;
}
