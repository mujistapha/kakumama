#include <stdio.h>

typedef struct Block {
    uint32_t version;
    char previousblockhash[64];
    char merkleroot[64];
    uint32_t time;
    char bits[8];
    uint32_t nonce;
} Block;

void pack_header(uint8_t* header, Block block)
{
    int offset = 0;
    uint32_t tmp;

    // Pack version
    tmp = __builtin_bswap32(block.version);
    memcpy(&header[offset], &tmp, sizeof(tmp));
    offset += sizeof(tmp);

    // Pack previous block hash
    for (int i = 0; i < 32; i++) {
        header[offset + i] = (uint8_t)block.previousblockhash[62 - (i * 2)];
        header[offset + i + 1] = (uint8_t)block.previousblockhash[63 - (i * 2)];
    }
    offset += 32;

    // Pack merkle root
    for (int i = 0; i < 32; i++) {
        header[offset + i] = (uint8_t)block.merkleroot[62 - (i * 2)];
        header[offset + i + 1] = (uint8_t)block.merkleroot[63 - (i * 2)];
    }
    offset += 32;

    // Pack time
    tmp = __builtin_bswap32(block.time);
    memcpy(&header[offset], &tmp, sizeof(tmp));
    offset += sizeof(tmp);

    // Pack bits
    for (int i = 0; i < 4; i++) {
        header[offset + i] = (uint8_t)block.bits[6 - (i * 2)];
        header[offset + i + 1] = (uint8_t)block.bits[7 - (i * 2)];
    }
    offset += 4;

    // Pack nonce
    tmp = __builtin_bswap32(block.nonce);
    memcpy(&header[offset], &tmp, sizeof(tmp));
}