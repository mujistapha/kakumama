import pycuda.driver as cuda
import pycuda.autoinit
from pycuda.compiler import SourceModule
import numpy as np
import hashlib

import binascii

# Define CUDA kernel code as a string
cuda_kernel = """
#include <stdio.h>
#include <cuda.h>
#include <stdint.h>
#include "/opt/project/kakumama/c/sha256.cuh"

__global__ void sha256_kernel(BYTE *data, BYTE *hash, uint32_t len) {
    // Call SHA-256 functions from sha256.cuh to compute hash on GPU
    SHA256_CTX ctx;
    sha256_init(&ctx);
    sha256_update(&ctx, data, len);
    sha256_final(&ctx, hash);
}
"""

# Compile CUDA kernel code
mod = SourceModule(cuda_kernel, include_dirs=["./include", "/opt/project/kakumama/c/sha256.cuh"])

# Get kernel function
sha256_kernel = mod.get_function("sha256_kernel")

def sha256_cuda(data):
    # Define input data and hash arrays
    data = np.array(list(data), dtype=np.uint8)
    hash = np.zeros(32, dtype=np.uint8)

    # Allocate memory on GPU
    d_data = cuda.mem_alloc(data.nbytes)
    d_hash = cuda.mem_alloc(hash.nbytes)

    # Copy input data to GPU
    cuda.memcpy_htod(d_data, data)

    # Call the SHA-256 kernel
    sha256_kernel(d_data, d_hash, np.uint32(len(data)), block=(1,1,1), grid=(1,1))

    # Copy the resulting hash from GPU to host
    cuda.memcpy_dtoh(hash, d_hash)

    # Free memory on GPU
    d_data.free()
    d_hash.free()

    # Return the resulting hash
    return hash

data = b"hello world"
hash = sha256_cuda(data)
print(type(hash))
hash1 = binascii.hexlify(hash).decode()
hash2 = hashlib.sha256(data).hexdigest()
print("SHA-256 hash cuda:",hash1)
print("SHA-256 hash hlib:",hash2)



