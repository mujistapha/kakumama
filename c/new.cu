#include <stdio.h>
#include <cuda.h>
#include "sha256.cuh"

__global__ void sha256_kernel(BYTE *data, BYTE *hash, uint32_t len) {
    // Call SHA-256 functions from sha256.cuh to compute hash on GPU
    SHA256_CTX ctx;
    sha256_init(&ctx);
    sha256_update(&ctx, data, len);
    sha256_final(&ctx, hash);
}

int main() {
    // Define input data and hash arrays
    BYTE data[] = {0x68, 0x65, 0x6c, 0x6c, 0x6f, 0x20, 0x77, 0x6f, 0x72, 0x6c, 0x64};
    BYTE hash[32];

    // Allocate memory on GPU
    BYTE *d_data, *d_hash;
    cudaMalloc(&d_data, sizeof(data));
    cudaMalloc(&d_hash, sizeof(hash));

    // Copy input data to GPU
    cudaMemcpy(d_data, data, sizeof(data), cudaMemcpyHostToDevice);

    // Call the SHA-256 kernel
    sha256_kernel<<<1, 1>>>(d_data, d_hash, sizeof(data));

    // Copy the resulting hash from GPU to host
    cudaMemcpy(hash, d_hash, 32, cudaMemcpyDeviceToHost);

    // Print the resulting hash
    printf("SHA-256 hash:");
    for (int i = 0; i < 32; i++) {
        printf("%02x", hash[i]);
    }
    printf("\n");

    // Free memory on GPU
    cudaFree(d_data);
    cudaFree(d_hash);

    return 0;
}
