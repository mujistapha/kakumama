import ctypes
import pycuda.driver as cuda
import numpy as np


# Load the shared library
sha256 = ctypes.CDLL('./sha256_cuda.so')


@cuda.jit
def sha256_kernel(data, hash):
    ctx = sha256.SHA256_CTX()
    sha256.sha256_init(ctx)
    sha256.sha256_update(ctx, data, len(data))
    sha256.sha256_update(ctx, data, len(data))
    sha256.sha256_final(ctx, hash)

# Define input data as a byte string
data_bytes = b'Hello World'

# Convert to a numpy array of 32-bit integers
data = np.frombuffer(data_bytes, dtype=np.uint32)

# Allocate memory for the hash
hash = np.empty(8, dtype=np.uint32)

# Call the kernel
sha256_kernel[1, 1](data, hash)

# Print the hash
print('SHA-256 hash:', hash)