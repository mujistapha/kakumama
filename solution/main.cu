#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <cuda.h>
#include "sha256.cuh"

__device__ unsigned int nonceCounter = 0;

__global__ void sha256_kernel(BYTE *header, BYTE *hash, uint32_t len, unsigned char target[32], unsigned int *solution, int *blockContainsSolution) {

    //nonceCounter should be a variable accessible from both cpu and GPU
    unsigned int nonce = atomicAdd(&nonceCounter, 1);
    memcpy(header + 76, &nonce, 4);

    // Call device function compute_raw_hash to compute hash on GPU
    BYTE raw_hash[32];

    SHA256_CTX ctx;
    sha256_init(&ctx);
    sha256_update(&ctx, header, len);
    sha256_final(&ctx, raw_hash);
    sha256_final(&ctx, raw_hash);

    int is_valid = 1;
    for (int i = 0; i < 32; i++) {
        if (raw_hash[i] > target[i]) {
            is_valid = 0;
            break;
        } else if (raw_hash[i] < target[i]) {
            break;
        }
    }

    if (is_valid && atomicCAS(blockContainsSolution, 0, 1) == 0) {
        *solution = nonce;
    }
}

unsigned char* hexToBytes(const char* hex, size_t* length) {
    size_t hexLen = strlen(hex);
    *length = hexLen / 2;
    unsigned char* bytes = (unsigned char*) malloc(*length);
    for (size_t i = 0; i < hexLen; i += 2) {
        sscanf(hex + i, "%2hhx", &bytes[i / 2]);
    }
    return bytes;
}


void bytesToHex(const unsigned char* bytes, size_t length, char* hex) {
    for (size_t i = 0; i < length; i++) {
        sprintf(hex + (i * 2), "%02x", bytes[i]);
    }
}

void bits2target(const char* bits, unsigned char* target) {
    unsigned char bitsArr[4];
    sscanf(bits, "%08x", (unsigned int*) &bitsArr);
    unsigned char shift = bitsArr[0] - 3;
    memcpy(target + shift, bitsArr + 1, 3);
}

void make_header(const char* previousBlockHash, const char* merkleRoot, const char* bits, unsigned char* header) {
    unsigned char versionArr[4];
    sscanf("54525952", "%08x", (unsigned int*) &versionArr);
    memcpy(header, versionArr, 4);
    size_t length;
    unsigned char* previousBlockBytes = hexToBytes(previousBlockHash, &length);
    for (int i = 0; i < 32; i++) {
        header[4 + i] = previousBlockBytes[31 - i];
    }
    free(previousBlockBytes);
    unsigned char* merkleRootBytes = hexToBytes(merkleRoot, &length);
    for (int i = 0; i < 32; i++) {
        header[36 + i] = merkleRootBytes[31 - i];
    }
    free(merkleRootBytes);
    sscanf(bits, "%08x", (unsigned int*) (header + 68));
}



void nonceFinder() {
    const char* previousBlockHash = "0000000000000000000b5060c3f578edcad8ff1e76df460d96227e34228d8420";
    const char* merkleRoot = "19aec47aaa99320f2a3b6f76b35d08ba6808f7bd0ff5920f949c29c6df613ed2";
    const char* bits = "170e2632";
    unsigned char header[80];
    unsigned char hash[32];
    unsigned char target[32];
    unsigned int max_nonce = 0xffffffff;
    size_t targetLen;

    bits2target(bits, target);

    double hash_rate = 0.0;
    int hash_rate_count = 0;
    time_t time_stamp = time(NULL);

    // Set up GPU context
    cudaError_t cudaStatus = cudaSetDevice(0);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
        return;
    }

    make_header(previousBlockHash, merkleRoot, bits, header);

    // Allocate memory on GPU
    BYTE *d_header, *d_target, *d_hash;
    unsigned int *d_solution, *h_solution;
    int *d_blockContainsSolution, *h_blockContainsSolution;
    cudaMalloc((void **)&d_header, 80);
    cudaMalloc((void **)&d_target, 32);
    cudaMalloc((void **)&d_hash, 32);
    cudaMalloc((void **)&d_solution, sizeof(unsigned int));
    cudaMalloc((void **)&d_blockContainsSolution, sizeof(int));
    h_solution = (unsigned int*) malloc(sizeof(unsigned int));
    h_blockContainsSolution = (int*) malloc(sizeof(int));
    *h_blockContainsSolution = 0;

    // Copy target to GPU
    cudaMemcpy(d_header, header, 80, cudaMemcpyHostToDevice);
    cudaMemcpy(d_target, target, 32, cudaMemcpyHostToDevice);

    // Assign initial value to nonceCounter
    unsigned int initialNonce = 2422853640;
    cudaMemcpyToSymbol(nonceCounter, &initialNonce, sizeof(unsigned int));

    // Call kernel to search for nonce
    int threadsPerBlock = 1;
    int blocksPerGrid = 1;
    sha256_kernel<<<blocksPerGrid, threadsPerBlock>>>(d_header, d_hash, 76, d_target, d_solution, d_blockContainsSolution);

    // Copy solution and blockContainsSolution from GPU to CPU
    cudaMemcpy(h_solution, d_solution, sizeof(unsigned int), cudaMemcpyDeviceToHost);
    cudaMemcpy(h_blockContainsSolution, d_blockContainsSolution, sizeof(int), cudaMemcpyDeviceToHost);

//     while (*h_blockContainsSolution == 0) {
//         // Calculate hash rate every 100,000 iterations
//         hash_rate_count++;
//         if (hash_rate_count == 100000) {
//             double elapsed_time = difftime(time(NULL), time_stamp);
//             hash_rate = hash_rate_count / elapsed_time;
//             printf("Hash rate: %.2lf hashes/s\n", hash_rate);
//             hash_rate_count = 0;
//             time_stamp = time(NULL);
//         }
//     }

    // Print solution
    printf("Nonce: %u\n", *h_solution);

    // Free memory
    cudaFree(d_header);
    cudaFree(d_target);
    cudaFree(d_hash);
    cudaFree(d_solution);
    cudaFree(d_blockContainsSolution);
    free(h_solution);
    free(h_blockContainsSolution);
}



int main() {
    nonceFinder();
    return 0;
}