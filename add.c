//sample C file to add 2 numbers - int and floats

#include <stdio.h>

void display(char* str, int age){
    printf("My name is %s and my age is %d", str, age);
}

int add_int(int, int);
float add_float(float, float);

int add_int(int num1, int num2){
    return num1 + num2;
}

float add_float(float num1, float num2){
    return num1 + num2;
}