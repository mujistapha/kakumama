#include <stdio.h>
#include <openssl/sha.h>

typedef struct {
    uint32_t version;
    uint8_t prev_block_hash[32];
    uint8_t merkle_root_hash[32];
    uint32_t timestamp;
    uint32_t bits;
    uint32_t nonce;
} Block;

int main() {
    // Initialize block data
    Block block = {
        545259524,
        {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0b, 0x50, 0x60, 0xc3, 0xf5, 0x78, 0xed, 0xca, 0xd8, 0xff, 0x1e, 0x76, 0xdf, 0x46, 0x0d, 0x96, 0x22, 0x7e, 0x34, 0x22, 0x8d, 0x84, 0x20},
        {0x19, 0xae, 0xc4, 0x7a, 0xaa, 0x99, 0x32, 0x0f, 0x2a, 0x3b, 0x6f, 0x76, 0xb3, 0x5d, 0x08, 0xba, 0x68, 0x08, 0xf7, 0xbd, 0x0f, 0xf5, 0x92, 0x0f, 0x94, 0x9c, 0x29, 0xc6, 0xdf, 0x61, 0x3e, 0xd2},
        1634406908,
        0x170e2632,
        2422853641
    };

    // Concatenate block header fields
    uint8_t block_header[80];
    memcpy(block_header, &block.version, 4);
    memcpy(block_header+4, block.prev_block_hash, 32);
    memcpy(block_header+36, block.merkle_root_hash, 32);
    memcpy(block_header+68, &block.timestamp, 4);
    memcpy(block_header+72, &block.bits, 4);
    memcpy(block_header+76, &block.nonce, 4);

    // Compute double-SHA-256 hash of block header
    uint8_t hash1[SHA256_DIGEST_LENGTH];
    SHA256(block_header, 80, hash1);
    uint8_t hash2[SHA256_DIGEST_LENGTH];
    SHA256(hash1, SHA256_DIGEST_LENGTH, hash2);

    // Reverse byte order of hash2
    uint8_t block_hash[32];
    for (int i = 0; i < 32; i++) {
        block_hash[i] = hash2[31 - i];
    }

    // Print block hash in little-endian byte order
    printf("Block hash: ");
    for (int i = 0; i < 32; i++) {
        printf("%02x", block_hash[i]);
    }
    printf("\n");

    return 0;
}
