#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <sys/time.h>
#include <pthread.h>
#include <locale.h>
#include <stdint.h>


#include <sodium.h>



#define SHA256_BLOCK_SIZE 32
#define SHA256_DIGEST_SIZE 32
#define BYTE uint8_t

typedef struct {
    uint32_t version;
    char previousblockhash[65];
    char merkleroot[65];
    uint32_t time;
    char bits[9];
    uint32_t nonce;
} Block;

uint32_t nonce = 0;

uint32_t htobe32(uint32_t x) {
    uint32_t y;
    uint8_t* p_x = (uint8_t*)&x;
    uint8_t* p_y = (uint8_t*)&y;

    p_y[0] = p_x[3];
    p_y[1] = p_x[2];
    p_y[2] = p_x[1];
    p_y[3] = p_x[0];

    return y;
}

uint32_t htole32(uint32_t x) {
    uint32_t y;
    uint8_t* p_x = (uint8_t*)&x;
    uint8_t* p_y = (uint8_t*)&y;

    p_y[0] = p_x[0];
    p_y[1] = p_x[1];
    p_y[2] = p_x[2];
    p_y[3] = p_x[3];

    return y;
}

uint32_t bits2target(const char* bits_str) {
    // Convert the string representation of bits to a 32-bit integer
    uint32_t bits = (uint32_t)strtoul(bits_str, NULL, 16);

    // Extract the exponent and mantissa from the bits
    uint8_t exponent = (bits >> 24) & 0xff;
    uint32_t mantissa = bits & 0x7fffff;

    // Calculate the target
    uint32_t target = mantissa << (8 * (exponent - 3));

    return target;
}


void print_hex(const uint8_t* data, size_t len) {
    for (size_t i = 0; i < len; i++) {
        printf("\\x%02x", data[i]);
    }
    printf("\n");
}


void hex_to_bytes_(const char* hex, uint8_t* bytes, size_t hex_len) {
    // Reverse the byte order of the input string
    char reversed_hex[hex_len + 1];
    for (size_t i = 0; i < hex_len; i += 2) {
        reversed_hex[i] = hex[hex_len - i - 2];
        reversed_hex[i + 1] = hex[hex_len - i - 1];
    }
    reversed_hex[hex_len] = '\0';

    // Convert the reversed hex string to bytes
    if (sodium_hex2bin(bytes, 32, reversed_hex, hex_len, NULL,  NULL, NULL) == -1) {
        printf("Error: hex_to_bytes_ failed\n");
        exit(1);
    }
}




void long_to_bytes_(uint32_t val, char *bytes, int len) {
    printf("Header: b'");
    for (int i = 0; i < len; ++i) {
        bytes[i] = (val >> (8 * i)) & 0xff;
        printf("\\x%02x", bytes[i]);
    }
    printf("'\n");
}

uint32_t betoh32(uint32_t big_endian_value) {
    uint32_t result = ((big_endian_value & 0xFF000000) >> 24) |
                      ((big_endian_value & 0x00FF0000) >>  8) |
                      ((big_endian_value & 0x0000FF00) <<  8) |
                      ((big_endian_value & 0x000000FF) << 24);
    return result;
}


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <arpa/inet.h>

void makeHeader(Block* block, char bytes[]) {

    // Pack the version number
    uint32_t version_le = htole32(block->version);
    memcpy(bytes, &version_le, sizeof(uint32_t));

    // Pack the previous block hash
    char* prevBlockHash = block->previousblockhash;
    for (int i = 0; i < 32; i++) {
        char hex[3];
        hex[0] = prevBlockHash[i * 2];
        hex[1] = prevBlockHash[i * 2 + 1];
        hex[2] = '\0';
        unsigned char byte = (unsigned char) strtoul(hex, NULL, 16);
        bytes[4 + i] = byte;
    }
    // Reverse the order of the bytes
    for (int i = 0; i < 16; i++) {
        char tmp = bytes[4 + i];
        bytes[4 + i] = bytes[35 - i];
        bytes[35 - i] = tmp;
    }

    // Pack the merkle root
    char* merkleRoot = block->merkleroot;
    for (int i = 0; i < 32; i++) {
        char hex[3];
        hex[0] = merkleRoot[i * 2];
        hex[1] = merkleRoot[i * 2 + 1];
        hex[2] = '\0';
        unsigned char byte = (unsigned char) strtoul(hex, NULL, 16);
        bytes[36 + i] = byte;
    }
    // Reverse the order of the bytes
    for (int i = 0; i < 16; i++) {
        char tmp = bytes[36 + i];
        bytes[36 + i] = bytes[67 - i];
        bytes[67 - i] = tmp;
    }

    // Pack the time
    uint32_t time = htonl(block->time);
    memcpy(bytes + 68, &time, sizeof(time));

    // Pack the bits
    char* bits = block->bits;
    for (int i = 0; i < 4; i++) {
        char hex[3];
        hex[0] = bits[i * 2];
        hex[1] = bits[i * 2 + 1];
        hex[2] = '\0';
        unsigned char byte = (unsigned char) strtoul(hex, NULL, 16);
        bytes[72 + i] = byte;
    }
    // Reverse the order of the bytes
    for (int i = 0; i < 2; i++) {
        char tmp = bytes[72 + i];
        bytes[72 + i] = bytes[75 - i];
        bytes[75 - i] = tmp;
    }

    printf("version: ");
    print_hex((uint8_t*)bytes, 4);

    printf("previousblockhash: ");
    print_hex((uint8_t*)bytes + 4, 32);

    printf("merkleroot: ");
    print_hex((uint8_t*)bytes + 36, 32);

    printf("time: ");
    print_hex((uint8_t*)bytes + 68, 4);

    printf("bits: ");
    print_hex((uint8_t*)bytes + 72, 4);

    printf("Header: b'");
    for (int i = 0; i < 80; i++) {
        printf("\\x%02x", (unsigned char)bytes[i]);
    }
    printf("'\n");
}





int main() {
    Block block = {
        545259524,
        "0000000000000000000b5060c3f578edcad8ff1e76df460d96227e34228d8420",
        "19aec47aaa99320f2a3b6f76b35d08ba6808f7bd0ff5920f949c29c6df613ed2",
        1634406908,
        "170e2632",
        2422853641
    };

    char header[80];
    unsigned char hash[32];
    char header_out[80];

    makeHeader(&block, header);

    nonce = block.nonce;

    printf("Header: ");
    for (int i = 0; i < 80; i++) {
        printf("%02x", header_out[i]);
    }
    printf("\n");

    uint32_t num = 2422853640;
    uint32_t network_num = htonl(num);
    char* bytes = (char*)&network_num;
    char little_endian_bytes[4];
    for (int i = 0; i < 4; i++) {
        little_endian_bytes[i] = bytes[3 - i];
    }
    for (int i = 0; i < 4; i++) {
        printf("\\x%02x", (unsigned char)little_endian_bytes[i]);
    }
    printf("\n");


    return 0;
}





