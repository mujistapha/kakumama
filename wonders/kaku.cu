#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <cuda.h>
#include <sys/time.h>
#include <pthread.h>
#include <locale.h>
#include "sha256.cuh"
#include <iostream>
#include <string>
#include <cstring>
#include <cstdio>
#include <cstdint>
#include <cmath>
#include <endian.h>

#include <stdint.h>
#include <sodium.h>



#define SHA256_BLOCK_SIZE 32
#define SHA256_DIGEST_SIZE 32
#define BYTE uint8_t

typedef struct {
    uint32_t version;
    char previousblockhash[65];
    char merkleroot[65];
    uint32_t time;
    char bits[9];
    uint32_t nonce;
} Block;

uint32_t nonce = 0;

uint32_t bits2target(const char* bits_str) {
    // Convert the string representation of bits to a 32-bit integer
    uint32_t bits = (uint32_t)strtoul(bits_str, NULL, 16);

    // Extract the exponent and mantissa from the bits
    uint8_t exponent = (bits >> 24) & 0xff;
    uint32_t mantissa = bits & 0x7fffff;

    // Calculate the target
    uint32_t target = mantissa << (8 * (exponent - 3));

    return target;
}


__device__ void hex_to_bytes(const char* hex, uint8_t* bytes, size_t len) {
    for (size_t i = 0; i < len; i++) {
        char c = hex[i];
        uint8_t value;
        if (c >= '0' && c <= '9') {
            value = c - '0';
        } else if (c >= 'a' && c <= 'f') {
            value = 10 + c - 'a';
        } else if (c >= 'A' && c <= 'F') {
            value = 10 + c - 'A';
        } else {
            // Handle invalid character
            return;
        }
        if (i % 2 == 0) {
            bytes[i/2] = value << 4;
        } else {
            bytes[i/2] |= value;
        }
    }
}



__device__ void long_to_bytes(uint32_t val, unsigned char* bytes, int len) {
    for (int i = 0; i < len; i++) {
        bytes[i] = (val >> ((len - i - 1) * 8)) & 0xFF;
    }
}

void print_hex(const uint8_t* data, size_t len) {
    for (size_t i = 0; i < len; i++) {
        printf("\\x%02x", data[i]);
    }
    printf("\n");
}


void hex_to_bytes_(const char* hex, uint8_t* bytes, size_t hex_len) {
    // Reverse the byte order of the input string
    char reversed_hex[hex_len + 1];
    for (size_t i = 0; i < hex_len; i += 2) {
        reversed_hex[i] = hex[hex_len - i - 2];
        reversed_hex[i + 1] = hex[hex_len - i - 1];
    }
    reversed_hex[hex_len] = '\0';

    // Convert the reversed hex string to bytes
    if (sodium_hex2bin(bytes, 32, reversed_hex, hex_len, NULL,  NULL, NULL) == -1) {
        printf("Error: hex_to_bytes_ failed\n");
        exit(1);
    }
}




void long_to_bytes_(uint32_t val, char *bytes, int len) {
    printf("Header: b'");
    for (int i = 0; i < len; ++i) {
        bytes[i] = (val >> (8 * i)) & 0xff;
        printf("\\x%02x", bytes[i]);
    }
    printf("'\n");
}

uint32_t betoh32(uint32_t big_endian_value) {
    uint32_t result = ((big_endian_value & 0xFF000000) >> 24) |
                      ((big_endian_value & 0x00FF0000) >>  8) |
                      ((big_endian_value & 0x0000FF00) <<  8) |
                      ((big_endian_value & 0x000000FF) << 24);
    return result;
}




void makeHeader(Block* block, char bytes[]) {
    // Convert version to little-endian bytes
    uint32_t version_le = htole32(block->version);
    memcpy(bytes, &version_le, sizeof(uint32_t));

     // Reverse the byte order of previousblockhash
    char reversed_prev_blockhash[64];
    for (size_t i = 0; i < 64; i += 2) {
        reversed_prev_blockhash[i] = block->previousblockhash[63 - i - 1];
        reversed_prev_blockhash[i + 1] = block->previousblockhash[63 - i];
    }
    reversed_prev_blockhash[64] = '\0';

    // Convert reversed_prev_blockhash to bytes
    hex_to_bytes_((const char*)reversed_prev_blockhash, (uint8_t*)bytes + 4, 32);

    // Convert merkleroot to bytes
    hex_to_bytes_(block->merkleroot, (uint8_t*)bytes + 36, 32);

    // Convert time to big-endian bytes
    uint32_t time_be = htobe32(block->time);
    memcpy(bytes + 68, &time_be, sizeof(uint32_t));

    // Convert bits to little-endian bytes
    uint32_t bits_le = htole32(strtol(block->bits, NULL, 16));
    memcpy(bytes + 72, &bits_le, sizeof(uint32_t));

    printf("version: ");
    print_hex((uint8_t*)bytes, 4);

    printf("previousblockhash: ");
    print_hex((uint8_t*)bytes + 4, 32);

    printf("merkleroot: ");
    print_hex((uint8_t*)bytes + 36, 32);

    printf("time: ");
    print_hex((uint8_t*)bytes + 68, 4);

    printf("bits: ");
    print_hex((uint8_t*)bytes + 72, 4);

    printf("Header: b'");
    for (int i = 0; i < 80; i++) {
        printf("\\x%02x", (unsigned char)bytes[i]);
    }
    printf("'\n");
}








__device__ uint32_t bytes_to_uint32(unsigned char *bytes) {
    return (bytes[0] << 24) | (bytes[1] << 16) | (bytes[2] << 8) | bytes[3];
}


__global__ void compute_raw_hash(unsigned char *header_in, uint32_t nonce, unsigned char *header_out, unsigned char *hash, uint32_t target) {
    // Update header with nonce
    long_to_bytes(nonce, header_out+76, 4);


    // Compute hash
    SHA256_CTX ctx;
    BYTE digest[32];

    sha256_init(&ctx);
    sha256_update(&ctx, header_out, 80);
    sha256_final(&ctx, digest);

    sha256_update(&ctx, digest, 80);
    sha256_final(&ctx, digest);

     // Check if hash meets the target
    uint32_t hash_int = bytes_to_uint32(digest);
    if (hash_int <= target) {
        // Copy hash to output buffer
        memcpy(hash, digest, 32);
    }

}



int main() {
    Block block = {
        545259524,
        "0000000000000000000b5060c3f578edcad8ff1e76df460d96227e34228d8420",
        "19aec47aaa99320f2a3b6f76b35d08ba6808f7bd0ff5920f949c29c6df613ed2",
        1634406908,
        "170e2632",
        2422853641
    };

    char header[80];
    unsigned char hash[32];
    char header_out[80];

    makeHeader(&block, header);

    nonce = block.nonce;

    // Convert difficulty bits to target
    uint32_t target = bits2target(block.bits);

//     compute_raw_hash<<<1,1>>>(header, nonce, header_out, hash, target);

    std::printf("Header: ");
    for (int i = 0; i < 80; i++) {
        std::printf("%02x", header_out[i]);
    }
    std::printf("\n");

    std::printf("Hash: ");
    for (int i = 0; i < 32; i++) {
        std::printf("%02x", hash[i]);
    }
    std::printf("\n");

    return 0;
}





